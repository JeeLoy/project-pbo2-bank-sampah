--------------------------------------------------------
--  File created - Friday-December-13-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table HISTORY_BELI
--------------------------------------------------------

  CREATE TABLE "PROJECT"."HISTORY_BELI" 
   (	"TANGGAL" DATE, 
	"ID_NASABAH" VARCHAR2(20 BYTE), 
	"ID_SAMPAH" VARCHAR2(20 BYTE), 
	"BERAT" NUMBER, 
	"ID_TRANSAKSI" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table HISTORY_JUAL
--------------------------------------------------------

  CREATE TABLE "PROJECT"."HISTORY_JUAL" 
   (	"TANGGAL" DATE, 
	"NAMA_PENGEPUL" VARCHAR2(20 BYTE), 
	"BERAT" NUMBER, 
	"TOTAL" NUMBER, 
	"ID_SAMPAH" VARCHAR2(20 BYTE), 
	"ID_TRANSAKSI" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table HISTORY_TARIK
--------------------------------------------------------

  CREATE TABLE "PROJECT"."HISTORY_TARIK" 
   (	"TANGGAL" DATE, 
	"ID_NASABAH" VARCHAR2(20 BYTE), 
	"TARIK" NUMBER, 
	"SALDO_AKHIR" NUMBER, 
	"ID_TRANSAKSI" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table HISTORY_TRANSAKSI
--------------------------------------------------------

  CREATE TABLE "PROJECT"."HISTORY_TRANSAKSI" 
   (	"ID_TRANSAKSI" NUMBER, 
	"TANGGAL" DATE, 
	"ID_NASABAH" VARCHAR2(20 BYTE), 
	"ID_SAMPAH" VARCHAR2(20 BYTE), 
	"SALDO" VARCHAR2(20 BYTE), 
	"STOCK" VARCHAR2(20 BYTE), 
	"KETERANGAN" VARCHAR2(20 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Table NASABAH
--------------------------------------------------------

  CREATE TABLE "PROJECT"."NASABAH" 
   (	"ID_NASABAH" VARCHAR2(20 BYTE), 
	"NAMA" VARCHAR2(20 BYTE), 
	"ALAMAT" VARCHAR2(20 BYTE), 
	"SALDO" NUMBER
   ) ;
--------------------------------------------------------
--  DDL for Table SAMPAH
--------------------------------------------------------

  CREATE TABLE "PROJECT"."SAMPAH" 
   (	"ID_SAMPAH" VARCHAR2(20 BYTE), 
	"JENIS" VARCHAR2(20 BYTE), 
	"HARGA" NUMBER(*,0), 
	"STOCK" FLOAT(5)
   ) ;
--------------------------------------------------------
--  DDL for Table USERS
--------------------------------------------------------

  CREATE TABLE "PROJECT"."USERS" 
   (	"USERNAME" VARCHAR2(20 BYTE), 
	"PASS" VARCHAR2(20 BYTE), 
	"NAMA_DEPAN" VARCHAR2(20 BYTE), 
	"NAMA_BELAKANG" VARCHAR2(20 BYTE)
   ) ;
REM INSERTING into PROJECT.HISTORY_BELI
SET DEFINE OFF;
REM INSERTING into PROJECT.HISTORY_JUAL
SET DEFINE OFF;
REM INSERTING into PROJECT.HISTORY_TARIK
SET DEFINE OFF;
REM INSERTING into PROJECT.HISTORY_TRANSAKSI
SET DEFINE OFF;
REM INSERTING into PROJECT.NASABAH
SET DEFINE OFF;
Insert into PROJECT.NASABAH (ID_NASABAH,NAMA,ALAMAT,SALDO) values ('N002','Fajar','Godean',30750);
Insert into PROJECT.NASABAH (ID_NASABAH,NAMA,ALAMAT,SALDO) values ('N005','Saa','Meguwo',0);
Insert into PROJECT.NASABAH (ID_NASABAH,NAMA,ALAMAT,SALDO) values ('N001','Jee','Meguwo',201150);
Insert into PROJECT.NASABAH (ID_NASABAH,NAMA,ALAMAT,SALDO) values ('N003','Tiansi','Paingan',0);
REM INSERTING into PROJECT.SAMPAH
SET DEFINE OFF;
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S001','Kertas',2500,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S002','Plastik',2000,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S004','Botol Plastik',5000,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S005','Gelas Plastik',4000,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S007','Kaleng',6500,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S006','Botol Kaca',7500,0);
Insert into PROJECT.SAMPAH (ID_SAMPAH,JENIS,HARGA,STOCK) values ('S003','Kardus',3000,0);
REM INSERTING into PROJECT.USERS
SET DEFINE OFF;
Insert into PROJECT.USERS (USERNAME,PASS,NAMA_DEPAN,NAMA_BELAKANG) values ('admin','admin','admin','admin');
Insert into PROJECT.USERS (USERNAME,PASS,NAMA_DEPAN,NAMA_BELAKANG) values ('JeeLoy','pqpq','Loyard','Jee');
--------------------------------------------------------
--  DDL for Index TABLE1_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROJECT"."TABLE1_PK" ON "PROJECT"."HISTORY_TRANSAKSI" ("ID_TRANSAKSI") 
  ;
--------------------------------------------------------
--  DDL for Index HISTORY_TARIK_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROJECT"."HISTORY_TARIK_PK" ON "PROJECT"."HISTORY_TARIK" ("ID_TRANSAKSI") 
  ;
--------------------------------------------------------
--  DDL for Index HISTORY_BELI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROJECT"."HISTORY_BELI_PK" ON "PROJECT"."HISTORY_BELI" ("ID_TRANSAKSI") 
  ;
--------------------------------------------------------
--  DDL for Index HISTORY_JUAL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROJECT"."HISTORY_JUAL_PK" ON "PROJECT"."HISTORY_JUAL" ("ID_TRANSAKSI") 
  ;
--------------------------------------------------------
--  Constraints for Table HISTORY_TARIK
--------------------------------------------------------

  ALTER TABLE "PROJECT"."HISTORY_TARIK" ADD CONSTRAINT "HISTORY_TARIK_PK" PRIMARY KEY ("ID_TRANSAKSI") ENABLE;
 
  ALTER TABLE "PROJECT"."HISTORY_TARIK" MODIFY ("TANGGAL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TARIK" MODIFY ("ID_NASABAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TARIK" MODIFY ("TARIK" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TARIK" MODIFY ("SALDO_AKHIR" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TARIK" MODIFY ("ID_TRANSAKSI" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HISTORY_TRANSAKSI
--------------------------------------------------------

  ALTER TABLE "PROJECT"."HISTORY_TRANSAKSI" MODIFY ("ID_TRANSAKSI" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TRANSAKSI" MODIFY ("TANGGAL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TRANSAKSI" MODIFY ("KETERANGAN" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_TRANSAKSI" ADD CONSTRAINT "TABLE1_PK" PRIMARY KEY ("ID_TRANSAKSI") ENABLE;
--------------------------------------------------------
--  Constraints for Table SAMPAH
--------------------------------------------------------

  ALTER TABLE "PROJECT"."SAMPAH" MODIFY ("ID_SAMPAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."SAMPAH" MODIFY ("JENIS" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."SAMPAH" MODIFY ("HARGA" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."SAMPAH" MODIFY ("STOCK" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USERS
--------------------------------------------------------

  ALTER TABLE "PROJECT"."USERS" MODIFY ("USERNAME" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."USERS" MODIFY ("PASS" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."USERS" MODIFY ("NAMA_DEPAN" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."USERS" MODIFY ("NAMA_BELAKANG" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HISTORY_BELI
--------------------------------------------------------

  ALTER TABLE "PROJECT"."HISTORY_BELI" ADD CONSTRAINT "HISTORY_BELI_PK" PRIMARY KEY ("ID_TRANSAKSI") ENABLE;
 
  ALTER TABLE "PROJECT"."HISTORY_BELI" MODIFY ("TANGGAL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_BELI" MODIFY ("ID_NASABAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_BELI" MODIFY ("ID_SAMPAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_BELI" MODIFY ("BERAT" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_BELI" MODIFY ("ID_TRANSAKSI" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table HISTORY_JUAL
--------------------------------------------------------

  ALTER TABLE "PROJECT"."HISTORY_JUAL" ADD CONSTRAINT "HISTORY_JUAL_PK" PRIMARY KEY ("ID_TRANSAKSI") ENABLE;
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("TANGGAL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("NAMA_PENGEPUL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("BERAT" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("TOTAL" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("ID_SAMPAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."HISTORY_JUAL" MODIFY ("ID_TRANSAKSI" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table NASABAH
--------------------------------------------------------

  ALTER TABLE "PROJECT"."NASABAH" MODIFY ("ID_NASABAH" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."NASABAH" MODIFY ("NAMA" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."NASABAH" MODIFY ("ALAMAT" NOT NULL ENABLE);
 
  ALTER TABLE "PROJECT"."NASABAH" MODIFY ("SALDO" NOT NULL ENABLE);
