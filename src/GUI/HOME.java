package GUI;

import Connection.gConnection;
import java.awt.CardLayout;
import java.awt.Color;
import java.sql.*;
import java.time.LocalDate;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public final class HOME extends javax.swing.JFrame {

    private Connection conn = null;
    private Statement st = null;
    private PreparedStatement ps = null;
    private TableRowSorter<TableModel> rowSorter;
    private final CardLayout cardLayout;
    private int harga, saldo, untung, tarik, total;
    private float stock, sisa, berat;
    private int trnsCrdsIndx = 1;
    private int menuCrdIndx;
    private Login login;
    String modeSekarang;
    boolean firstTime;

    public HOME() {
        initComponents();
        cardLayout = (CardLayout) (Points.getLayout());
        cardLayout.show(Points, "transaksiLight");
        jPanelAllHistory.setVisible(false);
        jPanelAbout.setVisible(false);
        modeSekarang = "light";
        firstTime = true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        Menu = new javax.swing.JPanel();
        jButtonTransaksi = new javax.swing.JButton();
        jButtonNasabah = new javax.swing.JButton();
        jButtonSampah = new javax.swing.JButton();
        jPanelMenuLight = new javax.swing.JLabel();
        jPanelMenuDark = new javax.swing.JLabel();
        Points = new javax.swing.JPanel();
        transaksiLight = new javax.swing.JPanel();
        jButtonBeliLight = new javax.swing.JButton();
        jButtonJualLight = new javax.swing.JButton();
        jButtonTarikLight = new javax.swing.JButton();
        transaksiDark = new javax.swing.JPanel();
        jButtonBeliDark = new javax.swing.JButton();
        jButtonJualDark = new javax.swing.JButton();
        jButtonTarikDark = new javax.swing.JButton();
        nasabah = new javax.swing.JPanel();
        jTextFieldCariNasabah = new javax.swing.JTextField();
        jButtonTambahUser = new javax.swing.JButton();
        jButtonRefreshNasabah = new javax.swing.JButton();
        jButtonDeleteUser = new javax.swing.JButton();
        jScrollPaneNasabah = new javax.swing.JScrollPane();
        jTableNasabah = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        sampah = new javax.swing.JPanel();
        jTextFieldCariSampah = new javax.swing.JTextField();
        jButtonTambahSampah = new javax.swing.JButton();
        jButtonRefreshSampah = new javax.swing.JButton();
        jButtonDeleteSampah = new javax.swing.JButton();
        jScrollPaneSampah = new javax.swing.JScrollPane();
        jTableSampah = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        transaksi_Beli = new javax.swing.JPanel();
        jLabelHistoryBeli = new javax.swing.JLabel();
        inputBeli = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldIDNasabahBeli = new javax.swing.JTextField();
        jTextFieldIDSampahBeli = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldBeratBeli = new javax.swing.JTextField();
        jButtonTransaksiBeli = new javax.swing.JButton();
        showDataBeli = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabelNamaBeli = new javax.swing.JLabel();
        jLabelJenisBeli = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabelHargaBeli = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabelTotalBeli = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jScrollPaneBeli = new javax.swing.JScrollPane();
        jTableHistoryBeli = new javax.swing.JTable();
        jButtonBackBeli = new javax.swing.JButton();
        transaksi_Jual = new javax.swing.JPanel();
        jLabel1HistoryJual = new javax.swing.JLabel();
        inputJual = new javax.swing.JPanel();
        jLabelNamaPengepul = new javax.swing.JLabel();
        jLabelIdSampah = new javax.swing.JLabel();
        jTextFieldNamaPengepulJual = new javax.swing.JTextField();
        jTextFieldIDSampahJual = new javax.swing.JTextField();
        jLabelBeratSampah = new javax.swing.JLabel();
        jTextFieldBeratJual = new javax.swing.JTextField();
        jButtonTransaksiJual = new javax.swing.JButton();
        showDataJual = new javax.swing.JPanel();
        jLabelJenisJual = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabelTotalJual = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabelHargaJual = new javax.swing.JLabel();
        jLabelStockJual = new javax.swing.JLabel();
        jScrollPaneHistoryJual = new javax.swing.JScrollPane();
        jTableHistoryJual = new javax.swing.JTable();
        jButtonBackJual = new javax.swing.JButton();
        transaksi_Tarik = new javax.swing.JPanel();
        inputTarik = new javax.swing.JPanel();
        jLabelUsernameTarik = new javax.swing.JLabel();
        jLabelJumlahTarik = new javax.swing.JLabel();
        jTextFieldIDNasabahTarik = new javax.swing.JTextField();
        jTextFieldJumlahTarik = new javax.swing.JTextField();
        jButtonTransaksiTarik = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextAreaAturanTarik = new javax.swing.JTextArea();
        jLabelHistoryTarik = new javax.swing.JLabel();
        showDataTarik = new javax.swing.JPanel();
        jLabel01 = new javax.swing.JLabel();
        jLabelNamaTarik = new javax.swing.JLabel();
        jLabelSaldoTarik = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabelSaldoAkhir = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        jScrollPaneHistoryBeli = new javax.swing.JScrollPane();
        jTableHistoryTarik = new javax.swing.JTable();
        jButtonBackTarik = new javax.swing.JButton();
        jPanelAllHistory = new javax.swing.JPanel();
        jButtonBackAllHistory = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAllHistory = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        tampilanDasarLight = new javax.swing.JLabel();
        tampilanDasarDark = new javax.swing.JLabel();
        jPanelAbout = new javax.swing.JPanel();
        jButtonBackAbout = new javax.swing.JButton();
        tampilanAbout = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuLainnya = new javax.swing.JMenu();
        jMenuMode = new javax.swing.JMenu();
        jMenuItemLightMode = new javax.swing.JMenuItem();
        jMenuItemDarkMode = new javax.swing.JMenuItem();
        jMenuItemHistories = new javax.swing.JMenuItem();
        jMenuItemAbout = new javax.swing.JMenuItem();
        jMenuLogout = new javax.swing.JMenu();

        jMenu5.setBackground(new java.awt.Color(255, 255, 255));
        jMenu5.setBorder(null);
        jMenu5.setForeground(new java.awt.Color(204, 0, 51));
        jMenu5.setText("Logout");
        jMenu5.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jMenu5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu5MouseClicked(evt);
            }
        });
        jMenu5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu5ActionPerformed(evt);
            }
        });
        jMenuBar2.add(jMenu5);

        jMenu6.setText("Lainnya");

        jMenuItem3.setText("Histori Semua Transaksi");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem3);

        jMenuItem4.setText("Tentang Kami");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem4);

        jMenuBar2.add(jMenu6);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1065, 760));
        setResizable(false);
        setSize(new java.awt.Dimension(1065, 760));
        getContentPane().setLayout(null);

        Menu.setBackground(new java.awt.Color(153, 153, 153));
        Menu.setForeground(new java.awt.Color(240, 240, 240));
        Menu.setMaximumSize(new java.awt.Dimension(139, 539));
        Menu.setOpaque(false);
        Menu.setLayout(null);

        jButtonTransaksi.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonTransaksi.setBorder(null);
        jButtonTransaksi.setBorderPainted(false);
        jButtonTransaksi.setContentAreaFilled(false);
        jButtonTransaksi.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button Transaksi.png"))); // NOI18N
        jButtonTransaksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTransaksiActionPerformed(evt);
            }
        });
        Menu.add(jButtonTransaksi);
        jButtonTransaksi.setBounds(10, 200, 150, 60);

        jButtonNasabah.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonNasabah.setBorder(null);
        jButtonNasabah.setBorderPainted(false);
        jButtonNasabah.setContentAreaFilled(false);
        jButtonNasabah.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button Nasabah.png"))); // NOI18N
        jButtonNasabah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNasabahActionPerformed(evt);
            }
        });
        Menu.add(jButtonNasabah);
        jButtonNasabah.setBounds(10, 280, 150, 60);

        jButtonSampah.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonSampah.setBorder(null);
        jButtonSampah.setBorderPainted(false);
        jButtonSampah.setContentAreaFilled(false);
        jButtonSampah.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button sampah.png"))); // NOI18N
        jButtonSampah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSampahActionPerformed(evt);
            }
        });
        Menu.add(jButtonSampah);
        jButtonSampah.setBounds(10, 360, 150, 60);

        jPanelMenuLight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Panel Menu.png"))); // NOI18N
        Menu.add(jPanelMenuLight);
        jPanelMenuLight.setBounds(-1, -1, 170, 540);

        jPanelMenuDark.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Panel Menu(dark).png"))); // NOI18N
        Menu.add(jPanelMenuDark);
        jPanelMenuDark.setBounds(-1, -1, 170, 540);

        getContentPane().add(Menu);
        Menu.setBounds(0, 150, 171, 538);

        Points.setBackground(new java.awt.Color(102, 102, 102));
        Points.setMaximumSize(new java.awt.Dimension(820, 538));
        Points.setMinimumSize(new java.awt.Dimension(820, 538));
        Points.setPreferredSize(new java.awt.Dimension(820, 538));
        Points.setLayout(new java.awt.CardLayout());

        transaksiLight.setBackground(new java.awt.Color(255, 255, 255));
        transaksiLight.setEnabled(false);
        transaksiLight.setMaximumSize(new java.awt.Dimension(820, 538));
        transaksiLight.setMinimumSize(new java.awt.Dimension(820, 538));
        transaksiLight.setPreferredSize(new java.awt.Dimension(820, 538));

        jButtonBeliLight.setBackground(new java.awt.Color(204, 204, 204));
        jButtonBeliLight.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonBeliLight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button beli sampah.png"))); // NOI18N
        jButtonBeliLight.setBorder(null);
        jButtonBeliLight.setContentAreaFilled(false);
        jButtonBeliLight.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button beli sampah.png"))); // NOI18N
        jButtonBeliLight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBeliLightActionPerformed(evt);
            }
        });

        jButtonJualLight.setBackground(new java.awt.Color(204, 204, 204));
        jButtonJualLight.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonJualLight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button jual samapah.png"))); // NOI18N
        jButtonJualLight.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(0, 0, 0), new java.awt.Color(204, 204, 204)));
        jButtonJualLight.setBorderPainted(false);
        jButtonJualLight.setContentAreaFilled(false);
        jButtonJualLight.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button jual sampah.png"))); // NOI18N
        jButtonJualLight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJualLightActionPerformed(evt);
            }
        });

        jButtonTarikLight.setBackground(new java.awt.Color(204, 204, 204));
        jButtonTarikLight.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonTarikLight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button penarikan.png"))); // NOI18N
        jButtonTarikLight.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(102, 102, 102), new java.awt.Color(204, 204, 204)));
        jButtonTarikLight.setBorderPainted(false);
        jButtonTarikLight.setContentAreaFilled(false);
        jButtonTarikLight.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button penarikan.png"))); // NOI18N
        jButtonTarikLight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTarikLightActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout transaksiLightLayout = new javax.swing.GroupLayout(transaksiLight);
        transaksiLight.setLayout(transaksiLightLayout);
        transaksiLightLayout.setHorizontalGroup(
            transaksiLightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksiLightLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jButtonBeliLight, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jButtonJualLight, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jButtonTarikLight, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        transaksiLightLayout.setVerticalGroup(
            transaksiLightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksiLightLayout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addGroup(transaksiLightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonTarikLight, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonJualLight, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBeliLight, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(183, Short.MAX_VALUE))
        );

        Points.add(transaksiLight, "transaksiLight");

        transaksiDark.setBackground(new java.awt.Color(0, 0, 0));
        transaksiDark.setEnabled(false);
        transaksiDark.setMaximumSize(new java.awt.Dimension(820, 538));
        transaksiDark.setMinimumSize(new java.awt.Dimension(820, 538));

        jButtonBeliDark.setBackground(new java.awt.Color(204, 204, 204));
        jButtonBeliDark.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonBeliDark.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button Beli(dark) - Copy.png"))); // NOI18N
        jButtonBeliDark.setBorder(null);
        jButtonBeliDark.setContentAreaFilled(false);
        jButtonBeliDark.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button Beli(dark).png"))); // NOI18N
        jButtonBeliDark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBeliDarkActionPerformed(evt);
            }
        });

        jButtonJualDark.setBackground(new java.awt.Color(204, 204, 204));
        jButtonJualDark.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonJualDark.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon Button Jual(dark).png"))); // NOI18N
        jButtonJualDark.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(0, 0, 0), new java.awt.Color(204, 204, 204)));
        jButtonJualDark.setBorderPainted(false);
        jButtonJualDark.setContentAreaFilled(false);
        jButtonJualDark.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover Button Jual(dark).png"))); // NOI18N
        jButtonJualDark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonJualDarkActionPerformed(evt);
            }
        });

        jButtonTarikDark.setBackground(new java.awt.Color(204, 204, 204));
        jButtonTarikDark.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonTarikDark.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button Penarikan(dark).png"))); // NOI18N
        jButtonTarikDark.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(204, 204, 204), new java.awt.Color(204, 204, 204), new java.awt.Color(102, 102, 102), new java.awt.Color(204, 204, 204)));
        jButtonTarikDark.setBorderPainted(false);
        jButtonTarikDark.setContentAreaFilled(false);
        jButtonTarikDark.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button Penarikan(dark).png"))); // NOI18N
        jButtonTarikDark.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTarikDarkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout transaksiDarkLayout = new javax.swing.GroupLayout(transaksiDark);
        transaksiDark.setLayout(transaksiDarkLayout);
        transaksiDarkLayout.setHorizontalGroup(
            transaksiDarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksiDarkLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jButtonBeliDark, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(jButtonJualDark, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(jButtonTarikDark, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        transaksiDarkLayout.setVerticalGroup(
            transaksiDarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksiDarkLayout.createSequentialGroup()
                .addGap(106, 106, 106)
                .addGroup(transaksiDarkLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonTarikDark, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonJualDark, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBeliDark, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(183, Short.MAX_VALUE))
        );

        Points.add(transaksiDark, "transaksiDark");

        nasabah.setBackground(new java.awt.Color(153, 153, 153));
        nasabah.setEnabled(false);
        nasabah.setMaximumSize(new java.awt.Dimension(820, 538));
        nasabah.setMinimumSize(new java.awt.Dimension(820, 538));
        nasabah.setPreferredSize(new java.awt.Dimension(820, 538));

        jTextFieldCariNasabah.setText("SEARCH");
        jTextFieldCariNasabah.setToolTipText("");
        jTextFieldCariNasabah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldCariNasabahMouseClicked(evt);
            }
        });
        jTextFieldCariNasabah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldCariNasabahKeyReleased(evt);
            }
        });

        jButtonTambahUser.setText("ADD");
        jButtonTambahUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTambahUserActionPerformed(evt);
            }
        });

        jButtonRefreshNasabah.setText("EDIT");
        jButtonRefreshNasabah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonEditNasabahMouseClicked(evt);
            }
        });

        jButtonDeleteUser.setBackground(new java.awt.Color(204, 0, 0));
        jButtonDeleteUser.setForeground(new java.awt.Color(255, 255, 255));
        jButtonDeleteUser.setText("DELETE");
        jButtonDeleteUser.setBorderPainted(false);
        jButtonDeleteUser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonDeleteUserMouseClicked(evt);
            }
        });

        jScrollPaneNasabah.setMaximumSize(new java.awt.Dimension(808, 397));
        jScrollPaneNasabah.setMinimumSize(new java.awt.Dimension(808, 397));
        jScrollPaneNasabah.setPreferredSize(new java.awt.Dimension(808, 397));

        jTableNasabah.setBackground(new java.awt.Color(0, 153, 153));
        jTableNasabah.setForeground(new java.awt.Color(255, 255, 255));
        jTableNasabah.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID NASABAH", "NAMA", "ALAMAT", "SALDO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableNasabah.setGridColor(new java.awt.Color(204, 204, 204));
        jTableNasabah.setMaximumSize(new java.awt.Dimension(300, 0));
        jTableNasabah.setMinimumSize(new java.awt.Dimension(300, 0));
        jTableNasabah.setOpaque(false);
        jTableNasabah.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableNasabah.setShowGrid(true);
        jTableNasabah.getTableHeader().setReorderingAllowed(false);
        jScrollPaneNasabah.setViewportView(jTableNasabah);

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("DAFTAR NASABAH");

        javax.swing.GroupLayout nasabahLayout = new javax.swing.GroupLayout(nasabah);
        nasabah.setLayout(nasabahLayout);
        nasabahLayout.setHorizontalGroup(
            nasabahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(nasabahLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(nasabahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(nasabahLayout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jButtonTambahUser)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonRefreshNasabah)
                        .addGap(241, 241, 241)
                        .addComponent(jButtonDeleteUser))
                    .addGroup(nasabahLayout.createSequentialGroup()
                        .addComponent(jTextFieldCariNasabah, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(163, 163, 163)
                        .addComponent(jLabel8)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPaneNasabah, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)
        );
        nasabahLayout.setVerticalGroup(
            nasabahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, nasabahLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(nasabahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCariNasabah, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jScrollPaneNasabah, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(nasabahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonTambahUser)
                    .addComponent(jButtonRefreshNasabah)
                    .addComponent(jButtonDeleteUser))
                .addGap(27, 27, 27))
        );

        Points.add(nasabah, "nasabah");

        sampah.setBackground(new java.awt.Color(153, 153, 153));
        sampah.setEnabled(false);
        sampah.setMaximumSize(new java.awt.Dimension(820, 538));
        sampah.setMinimumSize(new java.awt.Dimension(820, 538));
        sampah.setPreferredSize(new java.awt.Dimension(820, 538));

        jTextFieldCariSampah.setText("SEARCH");
        jTextFieldCariSampah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldCariSampahMouseClicked(evt);
            }
        });
        jTextFieldCariSampah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldCariSampahKeyReleased(evt);
            }
        });

        jButtonTambahSampah.setText("ADD");
        jButtonTambahSampah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTambahSampahActionPerformed(evt);
            }
        });

        jButtonRefreshSampah.setText("EDIT");
        jButtonRefreshSampah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonEditSampahMouseClicked(evt);
            }
        });

        jButtonDeleteSampah.setBackground(new java.awt.Color(204, 0, 0));
        jButtonDeleteSampah.setForeground(new java.awt.Color(255, 255, 255));
        jButtonDeleteSampah.setText("DELETE");
        jButtonDeleteSampah.setBorderPainted(false);
        jButtonDeleteSampah.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonDeleteSampahMouseClicked(evt);
            }
        });

        jScrollPaneSampah.setMaximumSize(new java.awt.Dimension(808, 397));
        jScrollPaneSampah.setMinimumSize(new java.awt.Dimension(808, 397));
        jScrollPaneSampah.setPreferredSize(new java.awt.Dimension(808, 397));

        jTableSampah.setBackground(new java.awt.Color(0, 153, 153));
        jTableSampah.setBorder(new javax.swing.border.MatteBorder(null));
        jTableSampah.setForeground(new java.awt.Color(255, 255, 255));
        jTableSampah.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID SAMPAH", "JENIS SAMPAH", "HARGA", "STOCK"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableSampah.setGridColor(new java.awt.Color(204, 204, 204));
        jTableSampah.setMaximumSize(new java.awt.Dimension(300, 0));
        jTableSampah.setMinimumSize(new java.awt.Dimension(300, 0));
        jTableSampah.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableSampah.setShowGrid(true);
        jTableSampah.getTableHeader().setReorderingAllowed(false);
        jScrollPaneSampah.setViewportView(jTableSampah);

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("DAFTAR SAMPAH");

        javax.swing.GroupLayout sampahLayout = new javax.swing.GroupLayout(sampah);
        sampah.setLayout(sampahLayout);
        sampahLayout.setHorizontalGroup(
            sampahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sampahLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(sampahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sampahLayout.createSequentialGroup()
                        .addComponent(jTextFieldCariSampah, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(170, 170, 170)
                        .addComponent(jLabel10))
                    .addGroup(sampahLayout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jButtonTambahSampah)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonRefreshSampah)
                        .addGap(243, 243, 243)
                        .addComponent(jButtonDeleteSampah)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPaneSampah, javax.swing.GroupLayout.DEFAULT_SIZE, 850, Short.MAX_VALUE)
        );
        sampahLayout.setVerticalGroup(
            sampahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sampahLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sampahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCariSampah, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(26, 26, 26)
                .addComponent(jScrollPaneSampah, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sampahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonTambahSampah)
                    .addComponent(jButtonRefreshSampah)
                    .addComponent(jButtonDeleteSampah))
                .addGap(0, 26, Short.MAX_VALUE))
        );

        Points.add(sampah, "sampah");

        transaksi_Beli.setBackground(new java.awt.Color(84, 164, 166));
        transaksi_Beli.setMaximumSize(new java.awt.Dimension(820, 538));
        transaksi_Beli.setMinimumSize(new java.awt.Dimension(820, 538));
        transaksi_Beli.setPreferredSize(new java.awt.Dimension(820, 538));

        jLabelHistoryBeli.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabelHistoryBeli.setForeground(new java.awt.Color(255, 255, 255));
        jLabelHistoryBeli.setText("Beli Sampah");

        inputBeli.setBackground(new java.awt.Color(68, 130, 132));
        inputBeli.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inputBeli.setMaximumSize(new java.awt.Dimension(144, 427));
        inputBeli.setMinimumSize(new java.awt.Dimension(144, 427));
        inputBeli.setPreferredSize(new java.awt.Dimension(144, 427));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID NASABAH");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("ID SAMPAH");

        jTextFieldIDNasabahBeli.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldIDNasabahBeli.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldIDNasabahBeliKeyReleased(evt);
            }
        });

        jTextFieldIDSampahBeli.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldIDSampahBeli.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldIDSampahBeliKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("BERAT (KG)");

        jTextFieldBeratBeli.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldBeratBeli.setEnabled(false);
        jTextFieldBeratBeli.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldBeratBeliKeyReleased(evt);
            }
        });

        jButtonTransaksiBeli.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonTransaksiBeli.setText("BELI");
        jButtonTransaksiBeli.setEnabled(false);
        jButtonTransaksiBeli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTransaksiBeliActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout inputBeliLayout = new javax.swing.GroupLayout(inputBeli);
        inputBeli.setLayout(inputBeliLayout);
        inputBeliLayout.setHorizontalGroup(
            inputBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputBeliLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldIDSampahBeli)
                    .addComponent(jTextFieldBeratBeli)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputBeliLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(inputBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(inputBeliLayout.createSequentialGroup()
                        .addGroup(inputBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextFieldIDNasabahBeli, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jButtonTransaksiBeli, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        inputBeliLayout.setVerticalGroup(
            inputBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputBeliLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldIDNasabahBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldIDSampahBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldBeratBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonTransaksiBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
        );

        showDataBeli.setBackground(new java.awt.Color(68, 130, 132));
        showDataBeli.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        showDataBeli.setMaximumSize(new java.awt.Dimension(157, 427));
        showDataBeli.setMinimumSize(new java.awt.Dimension(157, 427));
        showDataBeli.setPreferredSize(new java.awt.Dimension(157, 427));
        showDataBeli.setRequestFocusEnabled(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("NAMA NASABAH");

        jLabelNamaBeli.setBackground(new java.awt.Color(0, 0, 0));
        jLabelNamaBeli.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNamaBeli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelNamaBeli.setOpaque(true);

        jLabelJenisBeli.setBackground(new java.awt.Color(0, 0, 0));
        jLabelJenisBeli.setForeground(new java.awt.Color(255, 255, 255));
        jLabelJenisBeli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelJenisBeli.setOpaque(true);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("JENIS SAMPAH");

        jLabelHargaBeli.setBackground(new java.awt.Color(0, 0, 0));
        jLabelHargaBeli.setForeground(new java.awt.Color(255, 255, 255));
        jLabelHargaBeli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelHargaBeli.setOpaque(true);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("HARGA PER KG");

        jLabelTotalBeli.setBackground(new java.awt.Color(0, 0, 0));
        jLabelTotalBeli.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTotalBeli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelTotalBeli.setOpaque(true);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("TOTAL");

        javax.swing.GroupLayout showDataBeliLayout = new javax.swing.GroupLayout(showDataBeli);
        showDataBeli.setLayout(showDataBeliLayout);
        showDataBeliLayout.setHorizontalGroup(
            showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataBeliLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showDataBeliLayout.createSequentialGroup()
                        .addGap(0, 13, Short.MAX_VALUE)
                        .addComponent(jLabelTotalBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(showDataBeliLayout.createSequentialGroup()
                        .addGroup(showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                .addComponent(jLabelNamaBeli, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelJenisBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelHargaBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        showDataBeliLayout.setVerticalGroup(
            showDataBeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataBeliLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNamaBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelJenisBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelHargaBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelTotalBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTableHistoryBeli.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TANGGAL", "ID NASABAH", "ID SAMPAH", "BERAT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableHistoryBeli.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableHistoryBeli.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTableHistoryBeli.setShowGrid(true);
        jScrollPaneBeli.setViewportView(jTableHistoryBeli);

        jButtonBackBeli.setText("<");
        jButtonBackBeli.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBackBeliMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout transaksi_BeliLayout = new javax.swing.GroupLayout(transaksi_Beli);
        transaksi_Beli.setLayout(transaksi_BeliLayout);
        transaksi_BeliLayout.setHorizontalGroup(
            transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksi_BeliLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(transaksi_BeliLayout.createSequentialGroup()
                        .addComponent(inputBeli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(showDataBeli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonBackBeli))
                .addGroup(transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(transaksi_BeliLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPaneBeli, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transaksi_BeliLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelHistoryBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(323, 323, 323))))
        );
        transaksi_BeliLayout.setVerticalGroup(
            transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transaksi_BeliLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelHistoryBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBackBeli))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(transaksi_BeliLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(showDataBeli, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
                    .addComponent(inputBeli, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
                    .addComponent(jScrollPaneBeli, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE))
                .addContainerGap())
        );

        Points.add(transaksi_Beli, "transaksi1");

        transaksi_Jual.setBackground(new java.awt.Color(84, 164, 166));
        transaksi_Jual.setMaximumSize(new java.awt.Dimension(820, 538));
        transaksi_Jual.setMinimumSize(new java.awt.Dimension(820, 538));
        transaksi_Jual.setPreferredSize(new java.awt.Dimension(820, 538));

        jLabel1HistoryJual.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1HistoryJual.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1HistoryJual.setText("Jual Sampah");

        inputJual.setBackground(new java.awt.Color(68, 130, 132));
        inputJual.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inputJual.setMaximumSize(new java.awt.Dimension(144, 427));
        inputJual.setMinimumSize(new java.awt.Dimension(144, 427));
        inputJual.setPreferredSize(new java.awt.Dimension(144, 427));

        jLabelNamaPengepul.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelNamaPengepul.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNamaPengepul.setText("NAMA PENGEPUL");

        jLabelIdSampah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelIdSampah.setForeground(new java.awt.Color(255, 255, 255));
        jLabelIdSampah.setText("ID SAMPAH");

        jTextFieldNamaPengepulJual.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldNamaPengepulJual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldNamaPengepulJualKeyReleased(evt);
            }
        });

        jTextFieldIDSampahJual.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldIDSampahJual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldIDSampahJualKeyReleased(evt);
            }
        });

        jLabelBeratSampah.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelBeratSampah.setForeground(new java.awt.Color(255, 255, 255));
        jLabelBeratSampah.setText("BERAT (KG)");

        jTextFieldBeratJual.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldBeratJual.setEnabled(false);
        jTextFieldBeratJual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldBeratJualKeyReleased(evt);
            }
        });

        jButtonTransaksiJual.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonTransaksiJual.setText("JUAL");
        jButtonTransaksiJual.setEnabled(false);
        jButtonTransaksiJual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTransaksiJualActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout inputJualLayout = new javax.swing.GroupLayout(inputJual);
        inputJual.setLayout(inputJualLayout);
        inputJualLayout.setHorizontalGroup(
            inputJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputJualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(inputJualLayout.createSequentialGroup()
                        .addGroup(inputJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextFieldNamaPengepulJual, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNamaPengepul, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jTextFieldIDSampahJual)
                    .addComponent(jTextFieldBeratJual)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputJualLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(inputJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelIdSampah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelBeratSampah, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButtonTransaksiJual, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        inputJualLayout.setVerticalGroup(
            inputJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputJualLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelNamaPengepul, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldNamaPengepulJual, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelIdSampah, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldIDSampahJual, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelBeratSampah, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldBeratJual, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 156, Short.MAX_VALUE)
                .addComponent(jButtonTransaksiJual, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        showDataJual.setBackground(new java.awt.Color(68, 130, 132));
        showDataJual.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        showDataJual.setMaximumSize(new java.awt.Dimension(157, 427));
        showDataJual.setMinimumSize(new java.awt.Dimension(157, 427));
        showDataJual.setPreferredSize(new java.awt.Dimension(157, 427));

        jLabelJenisJual.setBackground(new java.awt.Color(0, 0, 0));
        jLabelJenisJual.setForeground(new java.awt.Color(255, 255, 255));
        jLabelJenisJual.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelJenisJual.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabelJenisJual.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabelJenisJual.setOpaque(true);
        jLabelJenisJual.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 255, 255));
        jLabel20.setText("JENIS SAMPAH");
        jLabel20.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabel20.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabel20.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("HARGA PER KG");
        jLabel22.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabel22.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabel22.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabelTotalJual.setBackground(new java.awt.Color(0, 0, 0));
        jLabelTotalJual.setForeground(new java.awt.Color(255, 255, 255));
        jLabelTotalJual.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelTotalJual.setOpaque(true);

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("TOTAL");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("STOCK");
        jLabel25.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabel25.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabel25.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabelHargaJual.setBackground(new java.awt.Color(0, 0, 0));
        jLabelHargaJual.setForeground(new java.awt.Color(255, 255, 255));
        jLabelHargaJual.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelHargaJual.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabelHargaJual.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabelHargaJual.setOpaque(true);
        jLabelHargaJual.setPreferredSize(new java.awt.Dimension(120, 30));

        jLabelStockJual.setBackground(new java.awt.Color(0, 0, 0));
        jLabelStockJual.setForeground(new java.awt.Color(255, 255, 255));
        jLabelStockJual.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelStockJual.setMaximumSize(new java.awt.Dimension(120, 30));
        jLabelStockJual.setMinimumSize(new java.awt.Dimension(120, 30));
        jLabelStockJual.setOpaque(true);
        jLabelStockJual.setPreferredSize(new java.awt.Dimension(120, 30));

        javax.swing.GroupLayout showDataJualLayout = new javax.swing.GroupLayout(showDataJual);
        showDataJual.setLayout(showDataJualLayout);
        showDataJualLayout.setHorizontalGroup(
            showDataJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataJualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(showDataJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(showDataJualLayout.createSequentialGroup()
                        .addGroup(showDataJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, showDataJualLayout.createSequentialGroup()
                        .addGroup(showDataJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelTotalJual, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelStockJual, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                            .addComponent(jLabelJenisJual, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelHargaJual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        showDataJualLayout.setVerticalGroup(
            showDataJualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataJualLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelJenisJual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelHargaJual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelStockJual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelTotalJual, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTableHistoryJual.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TANGGAL", "ID_SAMPAH", "NAMA PENGEPUL", "BERAT", "TOTAL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableHistoryJual.setGridColor(new java.awt.Color(153, 153, 153));
        jScrollPaneHistoryJual.setViewportView(jTableHistoryJual);

        jButtonBackJual.setText("<");
        jButtonBackJual.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBackJualMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout transaksi_JualLayout = new javax.swing.GroupLayout(transaksi_Jual);
        transaksi_Jual.setLayout(transaksi_JualLayout);
        transaksi_JualLayout.setHorizontalGroup(
            transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksi_JualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(inputJual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(transaksi_JualLayout.createSequentialGroup()
                        .addComponent(jButtonBackJual)
                        .addGap(289, 289, 289)
                        .addComponent(jLabel1HistoryJual, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(321, Short.MAX_VALUE))
            .addGroup(transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(transaksi_JualLayout.createSequentialGroup()
                    .addGap(164, 164, 164)
                    .addComponent(showDataJual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPaneHistoryJual, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        transaksi_JualLayout.setVerticalGroup(
            transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transaksi_JualLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonBackJual)
                    .addComponent(jLabel1HistoryJual, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(inputJual, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transaksi_JualLayout.createSequentialGroup()
                    .addContainerGap(66, Short.MAX_VALUE)
                    .addGroup(transaksi_JualLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(showDataJual, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                        .addComponent(jScrollPaneHistoryJual, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE))
                    .addContainerGap()))
        );

        Points.add(transaksi_Jual, "transaksi2");

        transaksi_Tarik.setBackground(new java.awt.Color(84, 164, 166));
        transaksi_Tarik.setMaximumSize(new java.awt.Dimension(820, 538));
        transaksi_Tarik.setMinimumSize(new java.awt.Dimension(820, 538));
        transaksi_Tarik.setPreferredSize(new java.awt.Dimension(820, 538));

        inputTarik.setBackground(new java.awt.Color(61, 128, 130));
        inputTarik.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        inputTarik.setMaximumSize(new java.awt.Dimension(144, 427));
        inputTarik.setMinimumSize(new java.awt.Dimension(144, 427));
        inputTarik.setPreferredSize(new java.awt.Dimension(144, 427));

        jLabelUsernameTarik.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelUsernameTarik.setForeground(new java.awt.Color(255, 255, 255));
        jLabelUsernameTarik.setText("ID NASABAH");

        jLabelJumlahTarik.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelJumlahTarik.setForeground(new java.awt.Color(255, 255, 255));
        jLabelJumlahTarik.setText("JUMLAH (Rp)");

        jTextFieldIDNasabahTarik.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldIDNasabahTarik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldIDNasabahTarikKeyReleased(evt);
            }
        });

        jTextFieldJumlahTarik.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextFieldJumlahTarik.setEnabled(false);
        jTextFieldJumlahTarik.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldJumlahTarikKeyReleased(evt);
            }
        });

        jButtonTransaksiTarik.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jButtonTransaksiTarik.setText("TARIK");
        jButtonTransaksiTarik.setEnabled(false);
        jButtonTransaksiTarik.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTransaksiTarikActionPerformed(evt);
            }
        });

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jTextAreaAturanTarik.setEditable(false);
        jTextAreaAturanTarik.setBackground(new java.awt.Color(0, 102, 102));
        jTextAreaAturanTarik.setColumns(20);
        jTextAreaAturanTarik.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        jTextAreaAturanTarik.setForeground(new java.awt.Color(255, 255, 255));
        jTextAreaAturanTarik.setRows(2);
        jTextAreaAturanTarik.setText("Jumlah penarikan minmal \nadalah Rp. 10000, serta harus \ndalam nilai Kelipatan 1000.");
        jScrollPane2.setViewportView(jTextAreaAturanTarik);

        javax.swing.GroupLayout inputTarikLayout = new javax.swing.GroupLayout(inputTarik);
        inputTarik.setLayout(inputTarikLayout);
        inputTarikLayout.setHorizontalGroup(
            inputTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputTarikLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldJumlahTarik)
                    .addComponent(jButtonTransaksiTarik, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputTarikLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jTextFieldIDNasabahTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(inputTarikLayout.createSequentialGroup()
                        .addGroup(inputTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelUsernameTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelJumlahTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        inputTarikLayout.setVerticalGroup(
            inputTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputTarikLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabelUsernameTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextFieldIDNasabahTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabelJumlahTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextFieldJumlahTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                .addComponent(jButtonTransaksiTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );

        jLabelHistoryTarik.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabelHistoryTarik.setForeground(new java.awt.Color(255, 255, 255));
        jLabelHistoryTarik.setText("Penarikan");

        showDataTarik.setBackground(new java.awt.Color(61, 128, 130));
        showDataTarik.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        showDataTarik.setMaximumSize(new java.awt.Dimension(157, 427));
        showDataTarik.setMinimumSize(new java.awt.Dimension(157, 427));
        showDataTarik.setPreferredSize(new java.awt.Dimension(157, 427));

        jLabel01.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel01.setForeground(new java.awt.Color(255, 255, 255));
        jLabel01.setText("NAMA NASABAH");

        jLabelNamaTarik.setBackground(new java.awt.Color(0, 0, 0));
        jLabelNamaTarik.setForeground(new java.awt.Color(255, 255, 255));
        jLabelNamaTarik.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelNamaTarik.setOpaque(true);

        jLabelSaldoTarik.setBackground(new java.awt.Color(0, 0, 0));
        jLabelSaldoTarik.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSaldoTarik.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelSaldoTarik.setOpaque(true);

        jLabel40.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(255, 255, 255));
        jLabel40.setText("SALDO");

        jLabelSaldoAkhir.setBackground(new java.awt.Color(0, 0, 0));
        jLabelSaldoAkhir.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSaldoAkhir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabelSaldoAkhir.setOpaque(true);

        jLabel44.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel44.setForeground(new java.awt.Color(255, 255, 255));
        jLabel44.setText("SISA SALDO");

        javax.swing.GroupLayout showDataTarikLayout = new javax.swing.GroupLayout(showDataTarik);
        showDataTarik.setLayout(showDataTarikLayout);
        showDataTarikLayout.setHorizontalGroup(
            showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataTarikLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelSaldoAkhir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(showDataTarikLayout.createSequentialGroup()
                        .addGroup(showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel01, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                .addComponent(jLabelNamaTarik, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel40, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabelSaldoTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 13, Short.MAX_VALUE)))
                .addContainerGap())
        );
        showDataTarikLayout.setVerticalGroup(
            showDataTarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(showDataTarikLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel01, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelNamaTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jLabelSaldoTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabelSaldoAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTableHistoryTarik.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TANGGAL", "ID NASABAH", "JUMLAH TARIK", "SALDO AKHIR"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableHistoryTarik.setGridColor(new java.awt.Color(153, 153, 153));
        jScrollPaneHistoryBeli.setViewportView(jTableHistoryTarik);

        jButtonBackTarik.setText("<");
        jButtonBackTarik.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBackTarikMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout transaksi_TarikLayout = new javax.swing.GroupLayout(transaksi_Tarik);
        transaksi_Tarik.setLayout(transaksi_TarikLayout);
        transaksi_TarikLayout.setHorizontalGroup(
            transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksi_TarikLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButtonBackTarik)
                .addGap(271, 271, 271)
                .addComponent(jLabelHistoryTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(396, Short.MAX_VALUE))
            .addGroup(transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(transaksi_TarikLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(inputTarik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(showDataTarik, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPaneHistoryBeli, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        transaksi_TarikLayout.setVerticalGroup(
            transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(transaksi_TarikLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonBackTarik)
                    .addComponent(jLabelHistoryTarik, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(497, Short.MAX_VALUE))
            .addGroup(transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, transaksi_TarikLayout.createSequentialGroup()
                    .addContainerGap(54, Short.MAX_VALUE)
                    .addGroup(transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jScrollPaneHistoryBeli, javax.swing.GroupLayout.PREFERRED_SIZE, 473, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(transaksi_TarikLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(inputTarik, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(showDataTarik, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap()))
        );

        Points.add(transaksi_Tarik, "transaksi3");
        transaksi_Tarik.getAccessibleContext().setAccessibleName("");

        getContentPane().add(Points);
        Points.setBounds(180, 150, 850, 538);

        jPanelAllHistory.setBackground(new java.awt.Color(255, 255, 255));
        jPanelAllHistory.setForeground(new java.awt.Color(0, 153, 153));

        jButtonBackAllHistory.setText("<<");
        jButtonBackAllHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBackAllHistoryActionPerformed(evt);
            }
        });

        jTableAllHistory.setBackground(new java.awt.Color(84, 164, 166));
        jTableAllHistory.setForeground(new java.awt.Color(255, 255, 255));
        jTableAllHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TANGGAL", "ID NASABAH", "ID SAMPAH", "SALDO NASABAH", "STOCK SAMPAH", "KETERANGAN"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableAllHistory.setGridColor(new java.awt.Color(0, 153, 153));
        jScrollPane1.setViewportView(jTableAllHistory);

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(84, 164, 166));
        jLabel5.setText("Semua Histori Transaksi");

        javax.swing.GroupLayout jPanelAllHistoryLayout = new javax.swing.GroupLayout(jPanelAllHistory);
        jPanelAllHistory.setLayout(jPanelAllHistoryLayout);
        jPanelAllHistoryLayout.setHorizontalGroup(
            jPanelAllHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAllHistoryLayout.createSequentialGroup()
                .addGroup(jPanelAllHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAllHistoryLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 970, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelAllHistoryLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonBackAllHistory)
                        .addGap(330, 330, 330)
                        .addComponent(jLabel5)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanelAllHistoryLayout.setVerticalGroup(
            jPanelAllHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAllHistoryLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelAllHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonBackAllHistory, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        getContentPane().add(jPanelAllHistory);
        jPanelAllHistory.setBounds(20, 132, 1010, 550);

        tampilanDasarLight.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Frame Dasar(light).png"))); // NOI18N
        tampilanDasarLight.setOpaque(true);
        getContentPane().add(tampilanDasarLight);
        tampilanDasarLight.setBounds(0, 0, 1050, 700);

        tampilanDasarDark.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Frame Dasar(dark).png"))); // NOI18N
        tampilanDasarDark.setOpaque(true);
        getContentPane().add(tampilanDasarDark);
        tampilanDasarDark.setBounds(0, 0, 1050, 700);

        jPanelAbout.setMaximumSize(new java.awt.Dimension(1050, 760));
        jPanelAbout.setMinimumSize(new java.awt.Dimension(1050, 760));
        jPanelAbout.setLayout(null);

        jButtonBackAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Icon button backAbout.png"))); // NOI18N
        jButtonBackAbout.setBorder(null);
        jButtonBackAbout.setBorderPainted(false);
        jButtonBackAbout.setContentAreaFilled(false);
        jButtonBackAbout.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Rollover button backAbout.png"))); // NOI18N
        jButtonBackAbout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonBackAboutMouseClicked(evt);
            }
        });
        jPanelAbout.add(jButtonBackAbout);
        jButtonBackAbout.setBounds(0, 20, 110, 21);

        tampilanAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/about.png"))); // NOI18N
        jPanelAbout.add(tampilanAbout);
        tampilanAbout.setBounds(0, 0, 1050, 700);

        getContentPane().add(jPanelAbout);
        jPanelAbout.setBounds(0, 0, 1050, 760);

        jMenuBar1.setBackground(new java.awt.Color(0, 0, 0));
        jMenuBar1.setInheritsPopupMenu(true);

        jMenuLainnya.setBackground(new java.awt.Color(0, 153, 153));
        jMenuLainnya.setText("Lainnya");

        jMenuMode.setText("Mode Tampilan");

        jMenuItemLightMode.setText("Light");
        jMenuItemLightMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLightModeActionPerformed(evt);
            }
        });
        jMenuMode.add(jMenuItemLightMode);

        jMenuItemDarkMode.setText("Drak");
        jMenuItemDarkMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDarkModeActionPerformed(evt);
            }
        });
        jMenuMode.add(jMenuItemDarkMode);

        jMenuLainnya.add(jMenuMode);

        jMenuItemHistories.setText("Histori Semua Transaksi");
        jMenuItemHistories.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHistoriesActionPerformed(evt);
            }
        });
        jMenuLainnya.add(jMenuItemHistories);

        jMenuItemAbout.setText("Tentang Kami");
        jMenuItemAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAboutActionPerformed(evt);
            }
        });
        jMenuLainnya.add(jMenuItemAbout);

        jMenuBar1.add(jMenuLainnya);

        jMenuLogout.setBackground(new java.awt.Color(255, 255, 255));
        jMenuLogout.setBorder(null);
        jMenuLogout.setForeground(new java.awt.Color(204, 0, 51));
        jMenuLogout.setText("Logout");
        jMenuLogout.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jMenuLogout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuLogoutMouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenuLogout);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSampahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSampahActionPerformed
        firstTime = false;
        menuCrdIndx = 7;
        reSam();
        cardLayout.show(Points, "sampah");
        jTextFieldCariSampah.setText("SEARCH");
    }//GEN-LAST:event_jButtonSampahActionPerformed

    private void jButtonNasabahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNasabahActionPerformed
        firstTime = false;
        menuCrdIndx = 6;
        reNas();
        cardLayout.show(Points, "nasabah");
        jTextFieldCariNasabah.setText("SEARCH");
    }//GEN-LAST:event_jButtonNasabahActionPerformed

    private void jButtonTransaksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTransaksiActionPerformed
        if (modeSekarang.equalsIgnoreCase("dark") && trnsCrdsIndx == 1) {
            trnsCrdsIndx = 2;
        } else if (modeSekarang.equalsIgnoreCase("light") && trnsCrdsIndx == 2) {
            trnsCrdsIndx = 1;
        }

        switch (trnsCrdsIndx) {
            case 1:
                firstTime = true;
                cardLayout.show(Points, "transaksiLight");
                break;
            case 2:
                firstTime = true;
                cardLayout.show(Points, "transaksiDark");
                break;
            case 3:
                cardLayout.show(Points, "transaksi1");

                break;
            case 4:
                cardLayout.show(Points, "transaksi2");

                break;
            case 5:
                cardLayout.show(Points, "transaksi3");
                break;
            default:
                break;
        }
    }//GEN-LAST:event_jButtonTransaksiActionPerformed

    private void jButtonTambahSampahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTambahSampahActionPerformed
        InputSampah is = new InputSampah(this);
        is.setVisible(true);
    }//GEN-LAST:event_jButtonTambahSampahActionPerformed

    private void jButtonTambahUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTambahUserActionPerformed
        InputNasabah in = new InputNasabah(this);
        in.setVisible(true);
    }//GEN-LAST:event_jButtonTambahUserActionPerformed

    private void jButtonDeleteUserMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonDeleteUserMouseClicked
        DefaultTableModel tb = (DefaultTableModel) jTableNasabah.getModel();
        int row = jTableNasabah.getSelectedRow();
        if (row >= 0) {
            int confir = JOptionPane.showConfirmDialog(null, "Apakah anda yakin?", null, JOptionPane.YES_NO_OPTION);
            if (confir == JOptionPane.YES_OPTION) {
                try {
                    conn = gConnection.getConnection();
                    ps = conn.prepareStatement("DELETE FROM nasabah WHERE id_nasabah = '" + (String) jTableNasabah.getValueAt(row, 0) + "'");
                    ps.executeUpdate();
                    conn.commit();
                    JOptionPane.showMessageDialog(null, "Data berhasil di hapus");
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            tb.removeRow(row);
        } else {
            JOptionPane.showMessageDialog(null, "Tidak ada yang dipilih\nSilahkan pilih salah satu");
        }
    }//GEN-LAST:event_jButtonDeleteUserMouseClicked

    private void jButtonDeleteSampahMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonDeleteSampahMouseClicked
        DefaultTableModel tb = (DefaultTableModel) jTableSampah.getModel();
        int row = jTableSampah.getSelectedRow();
        if (row >= 0) {
            int confir = JOptionPane.showConfirmDialog(null, "Apakah anda yakin?", null, JOptionPane.YES_NO_OPTION);
            if (confir == JOptionPane.YES_OPTION) {
                try {
                    conn = gConnection.getConnection();
                    ps = conn.prepareStatement("DELETE FROM sampah WHERE id_sampah = '" + (String) jTableSampah.getValueAt(row, 0) + "'");
                    ps.executeUpdate();
                    conn.commit();
                    JOptionPane.showMessageDialog(null, "Data berhasil di hapus");
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            tb.removeRow(row);
        } else {
            JOptionPane.showMessageDialog(null, "Tidak ada yang dipilih\nSilahkan pilih salah satu");
        }
    }//GEN-LAST:event_jButtonDeleteSampahMouseClicked

    private void jButtonEditNasabahMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEditNasabahMouseClicked
        DefaultTableModel tb = (DefaultTableModel) jTableNasabah.getModel();
        int row = jTableNasabah.getSelectedRow();
        if (row >= 0) {
            EditNasabah editNasabah = new EditNasabah(this, (String) jTableNasabah.getValueAt(row, 0), (String) jTableNasabah.getValueAt(row, 1), (String) jTableNasabah.getValueAt(row, 2));
            editNasabah.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Tidak ada yang dipilih\nSilahkan pilih salah satu");
        }
    }//GEN-LAST:event_jButtonEditNasabahMouseClicked

    private void jButtonEditSampahMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonEditSampahMouseClicked
        DefaultTableModel tb = (DefaultTableModel) jTableSampah.getModel();
        int row = jTableSampah.getSelectedRow();
        if (row >= 0) {
            EditSampah editSampah = new EditSampah(this, (String) jTableSampah.getValueAt(row, 0), (String) jTableSampah.getValueAt(row, 1), (int) jTableSampah.getValueAt(row, 2));
            editSampah.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Tidak ada yang dipilih\nSilahkan pilih salah satu");
        }
    }//GEN-LAST:event_jButtonEditSampahMouseClicked

    private void jButtonBeliLightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBeliLightActionPerformed
        firstTime = false;
        reTabung();
        cardLayout.show(Points, "transaksi1");
        trnsCrdsIndx = 3;
        menuCrdIndx = 3;
    }//GEN-LAST:event_jButtonBeliLightActionPerformed

    private void jButtonJualLightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonJualLightActionPerformed
        firstTime = false;
        reJual();
        cardLayout.show(Points, "transaksi2");
        trnsCrdsIndx = 4;
        menuCrdIndx = 4;
    }//GEN-LAST:event_jButtonJualLightActionPerformed

    private void jButtonTarikLightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTarikLightActionPerformed
        firstTime = false;
        reTarik();
        cardLayout.show(Points, "transaksi3");
        trnsCrdsIndx = 5;
        menuCrdIndx = 5;
    }//GEN-LAST:event_jButtonTarikLightActionPerformed

    private void jTextFieldIDNasabahBeliKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldIDNasabahBeliKeyReleased
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_nasabah, nama, saldo FROM nasabah");
            while (rs.next()) {
                if (jTextFieldIDNasabahBeli.getText().equals(rs.getString(1))) {
                    jLabelNamaBeli.setText(rs.getString(2));
                    saldo = rs.getInt(3);
                    if (!jLabelJenisBeli.getText().equals("")) {
                        jButtonTransaksiBeli.setEnabled(true);
                    }
                    break;
                } else {
                    jLabelNamaBeli.setText("");
                    jButtonTransaksiBeli.setEnabled(false);
                }
            }
            st.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_jTextFieldIDNasabahBeliKeyReleased

    private void jTextFieldIDSampahBeliKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldIDSampahBeliKeyReleased
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_sampah, jenis, harga, stock FROM sampah");
            while (rs.next()) {
                if (jTextFieldIDSampahBeli.getText().equals(rs.getString(1))) {
                    jLabelJenisBeli.setText(" " + rs.getString(2));
                    harga = rs.getInt(3);   //harga
                    stock = rs.getFloat(4);   //stock
                    jLabelHargaBeli.setText(" Rp." + harga);
                    jTextFieldBeratBeli.setEnabled(true);
                    if (berat > 0) {
                        total = (int) (harga * berat);
                        jLabelTotalBeli.setText("Rp. " + total);
                        jButtonTransaksiBeli.setEnabled(true);
                    }
                    break;
                } else {
                    jLabelJenisBeli.setText("");
                    jLabelHargaBeli.setText("");
                    jLabelTotalBeli.setText("");
                    jTextFieldBeratBeli.setEnabled(false);
                    jButtonTransaksiBeli.setEnabled(false);
                }
            }
            st.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_jTextFieldIDSampahBeliKeyReleased

    private void jTextFieldBeratBeliKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldBeratBeliKeyReleased
        try {
            berat = Float.valueOf(jTextFieldBeratBeli.getText()); //berat
            if (berat > 0) {
                total = (int) (harga * berat);  //total
                if (total >= 0) {
                    jLabelTotalBeli.setText("Rp. " + total);
                    if (jLabelNamaBeli.getText().equals("")) {
                        jButtonTransaksiBeli.setEnabled(false);
                    } else {
                        jButtonTransaksiBeli.setEnabled(true);
                    }
                }
            } else {
                jButtonTransaksiBeli.setEnabled(false);
            }

        } catch (NumberFormatException e) {
            berat = 0;
            jTextFieldBeratBeli.setText("");
            jLabelTotalBeli.setText("");
            jButtonTransaksiBeli.setEnabled(false);
        }
    }//GEN-LAST:event_jTextFieldBeratBeliKeyReleased

    private void jButtonTransaksiBeliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTransaksiBeliActionPerformed
        int confir = JOptionPane.showConfirmDialog(null, "Apakah Data Sudah Benar?", null, JOptionPane.YES_NO_OPTION);
        if (confir == JOptionPane.YES_OPTION) {
            int id_trans;
            conn = gConnection.getConnection();
            try {
                st = conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id_transaksi) FROM history_beli");
                if (rs.next()) {
                    id_trans = rs.getInt(1) + 1;
                } else {
                    id_trans = 1;
                }
                ps = conn.prepareStatement("INSERT INTO history_beli (id_transaksi, tanggal, id_nasabah, id_sampah, berat) VALUES (?,?,?,?,?)");
                ps.setInt(1, id_trans);
                ps.setDate(2, Date.valueOf(LocalDate.now()));
                ps.setString(3, jTextFieldIDNasabahBeli.getText());
                ps.setString(4, jTextFieldIDSampahBeli.getText());
                ps.setFloat(5, berat);
                ps.executeUpdate();
                conn.commit();
                updateSaldo(jTextFieldIDNasabahBeli.getText(), (saldo + total));
                updateStock(jTextFieldIDSampahBeli.getText(), (stock + berat));

                insertIntoTabelTrans(Date.valueOf(LocalDate.now()), jTextFieldIDNasabahBeli.getText(), jTextFieldIDSampahBeli.getText(), "+" + saldo, "+" + berat, "Pembelian Sampah");
                jTextFieldIDNasabahBeli.setText("");
                jTextFieldIDSampahBeli.setText("");
                jTextFieldBeratBeli.setText("");
                jLabelNamaBeli.setText("");
                jLabelJenisBeli.setText("");
                jLabelHargaBeli.setText("");
                jLabelTotalBeli.setText("");
                jButtonTransaksiBeli.setEnabled(false);
                saldo = total = harga = 0;
                stock = berat = 0;
                reTabung();
                st.close();
                ps.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jButtonTransaksiBeliActionPerformed

    private void jButtonBackBeliMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBackBeliMouseClicked
        firstTime = true;
        if (modeSekarang.equalsIgnoreCase("light")) {
            cardLayout.show(Points, "transaksiLight");
            trnsCrdsIndx = 1;
        } else {
            cardLayout.show(Points, "transaksiDark");
            trnsCrdsIndx = 2;
        }
        jTextFieldIDNasabahBeli.setText("");
        jTextFieldIDSampahBeli.setText("");
        jTextFieldBeratBeli.setText("");
        jLabelNamaBeli.setText("");
        jLabelJenisBeli.setText("");
        jLabelHargaBeli.setText("");
        jLabelTotalBeli.setText("");
        jButtonTransaksiBeli.setEnabled(false);
        saldo = total = harga = 0;
        stock = berat = 0;
    }//GEN-LAST:event_jButtonBackBeliMouseClicked

    private void jTextFieldNamaPengepulJualKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldNamaPengepulJualKeyReleased
        if (jLabelJenisJual.getText().equals("") && jLabelTotalJual.getText().equals("")) {
            jButtonTransaksiJual.setEnabled(false);
        } else {
            jButtonTransaksiJual.setEnabled(true);
        }
    }//GEN-LAST:event_jTextFieldNamaPengepulJualKeyReleased

    private void jTextFieldIDSampahJualKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldIDSampahJualKeyReleased
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_sampah,jenis,harga,stock FROM sampah");
            while (rs.next()) {
                if (jTextFieldIDSampahJual.getText().equals(rs.getString(1))) {
                    jLabelJenisJual.setText(" " + rs.getString(2));
                    jLabelHargaJual.setText(" Rp." + rs.getInt(3));
                    jLabelStockJual.setText(" " + rs.getInt(4) + " Kg");
                    harga = rs.getInt(3);  //harga
                    stock = rs.getFloat(4);  //stock
                    untung = harga * 10 / 100;
                    if (berat > 0) {
                        sisa = stock - berat;
                        total = (int) ((untung + harga) * berat);
                        if (sisa >= 0) {
                            jLabelTotalJual.setText("Rp. " + total);
                            jButtonTransaksiJual.setEnabled(true);
                        }
                    }
                    jTextFieldBeratJual.setEnabled(true);
                    if (jTextFieldNamaPengepulJual.getText().equals("")) {
                        jButtonTransaksiJual.setEnabled(false);
                    }
                    break;
                } else {
                    untung = 0;
                    jLabelJenisJual.setText("");
                    jLabelHargaJual.setText("");
                    jLabelStockJual.setText("");
                    jLabelTotalJual.setText("");
                    jTextFieldBeratJual.setEnabled(false);
                    jButtonTransaksiJual.setEnabled(false);
                }
            }
            st.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_jTextFieldIDSampahJualKeyReleased

    private void jTextFieldBeratJualKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldBeratJualKeyReleased
        try {
            berat = Float.valueOf(jTextFieldBeratJual.getText());   //berat
            if (berat > 0) {
                sisa = stock - berat;  //sisa
                if (sisa >= 0) {
                    total = (int) ((untung + harga) * berat);
                    jLabelTotalJual.setText("Rp. " + total);
                    jButtonTransaksiJual.setEnabled(true);
                } else {
                    jButtonTransaksiJual.setEnabled(false);
                }
            } else {
                jButtonTransaksiJual.setEnabled(false);
            }
            if (jTextFieldNamaPengepulJual.getText().equals("")) {
                jButtonTransaksiJual.setEnabled(false);
            }
        } catch (NumberFormatException e) {
            jTextFieldBeratJual.setText("");
            jLabelTotalJual.setText("");
            jButtonTransaksiJual.setEnabled(false);
        }
    }//GEN-LAST:event_jTextFieldBeratJualKeyReleased

    private void jButtonTransaksiJualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTransaksiJualActionPerformed

        int confir = JOptionPane.showConfirmDialog(null, "Apakah Data Sudah Benar?", null, JOptionPane.YES_NO_OPTION);
        if (confir == JOptionPane.YES_OPTION) {
            conn = gConnection.getConnection();
            int id_trans;
            try {
                st = conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id_transaksi) FROM history_jual");
                if (rs.next()) {
                    id_trans = rs.getInt(1) + 1;
                } else {
                    id_trans = 1;
                }
                ps = conn.prepareStatement("INSERT INTO HISTORY_JUAL (ID_TRANSAKSI, TANGGAL, NAMA_PENGEPUL, BERAT, TOTAL, ID_SAMPAH) VALUES (?,?,?,?,?,?)");
                ps.setInt(1, id_trans);
                ps.setDate(2, Date.valueOf(LocalDate.now()));
                ps.setString(3, jTextFieldNamaPengepulJual.getText());
                ps.setFloat(4, berat);
                ps.setInt(5, total);
                ps.setString(6, jTextFieldIDSampahJual.getText());
                ps.executeUpdate();
                conn.commit();
                updateStock(jTextFieldIDSampahJual.getText(), sisa);
                insertIntoTabelTrans(Date.valueOf(LocalDate.now()), "-", jTextFieldIDSampahJual.getText(), "-", "-" + berat, "Penjualan Sampah");
                jTextFieldNamaPengepulJual.setText("");
                jTextFieldIDSampahJual.setText("");
                jTextFieldBeratJual.setText("");
                jLabelJenisJual.setText("");
                jLabelStockJual.setText("");
                jLabelHargaJual.setText("");
                jLabelTotalJual.setText("");
                jButtonTransaksiJual.setEnabled(false);
                harga = untung = total = 0;
                stock = berat = sisa = 0;
                reJual();
                st.close();
                ps.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jButtonTransaksiJualActionPerformed

    private void jButtonBackJualMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBackJualMouseClicked
        firstTime = true;
        if (modeSekarang.equalsIgnoreCase("light")) {
            cardLayout.show(Points, "transaksiLight");
            trnsCrdsIndx = 1;
        } else {
            cardLayout.show(Points, "transaksiDark");
            trnsCrdsIndx = 2;
        }
        jTextFieldNamaPengepulJual.setText("");
        jTextFieldIDSampahJual.setText("");
        jTextFieldBeratJual.setText("");
        jLabelJenisJual.setText("");
        jLabelStockJual.setText("");
        jLabelHargaJual.setText("");
        jLabelTotalJual.setText("");
        jButtonTransaksiJual.setEnabled(false);
        harga = untung = total = 0;
        stock = berat = sisa = 0;
    }//GEN-LAST:event_jButtonBackJualMouseClicked

    private void jTextFieldIDNasabahTarikKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldIDNasabahTarikKeyReleased
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_nasabah, nama, saldo FROM nasabah");
            while (rs.next()) {
                if (jTextFieldIDNasabahTarik.getText().equals(rs.getString(1))) {
                    jLabelNamaTarik.setText(rs.getString(2));
                    jLabelSaldoTarik.setText("Rp." + rs.getInt(3));
                    jTextFieldJumlahTarik.setEnabled(true);
                    saldo = rs.getInt(3);   //saldo
                    if (tarik > 0) {
                        total = saldo - tarik;
                        if (total >= 0) {
                            jLabelSaldoAkhir.setText("Rp. " + total);
                            jButtonTransaksiTarik.setEnabled(true);
                        }
                    }
                    break;
                } else {
                    jLabelNamaTarik.setText("");
                    jLabelSaldoTarik.setText("");
                    jLabelSaldoAkhir.setText("");
                    jTextFieldJumlahTarik.setEnabled(false);
                    jButtonTransaksiTarik.setEnabled(false);
                }
            }
            st.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_jTextFieldIDNasabahTarikKeyReleased

    private void jTextFieldJumlahTarikKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldJumlahTarikKeyReleased
        try {
            tarik = Integer.valueOf(jTextFieldJumlahTarik.getText());   //jumlah tarik
            int check = tarik % 1000;
            if (tarik >= 10000 && check == 0) {
                total = saldo - tarik;  //saldo akhir
                if (total >= 0) {
                    jLabelSaldoAkhir.setText("Rp. " + total);
                    if (jLabelNamaTarik.getText().equals("")) {
                        jButtonTransaksiTarik.setEnabled(false);
                    } else {
                        jButtonTransaksiTarik.setEnabled(true);
                    }
                }
            } else {
                jLabelSaldoAkhir.setText("");
                jButtonTransaksiTarik.setEnabled(false);
            }
        } catch (NumberFormatException e) {
            jTextFieldJumlahTarik.setText("");
            jLabelSaldoAkhir.setText("");
            jButtonTransaksiTarik.setEnabled(false);
        }
    }//GEN-LAST:event_jTextFieldJumlahTarikKeyReleased

    private void jButtonTransaksiTarikActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTransaksiTarikActionPerformed
        int id_trans;
        int confir = JOptionPane.showConfirmDialog(null, "Apakah Data Sudah Benar?", null, JOptionPane.YES_NO_OPTION);
        if (confir == JOptionPane.YES_OPTION) {
            conn = gConnection.getConnection();
            try {
                st = conn.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id_transaksi) FROM history_tarik");
                if (rs.next()) {
                    id_trans = rs.getInt(1) + 1;
                } else {
                    id_trans = 1;
                }
                ps = conn.prepareStatement("INSERT INTO HISTORY_TARIK (ID_TRANSAKSI, TANGGAL, ID_NASABAH, TARIK, SALDO_AKHIR) VALUES (?,?,?,?,?)");
                ps.setInt(1, id_trans);
                ps.setDate(2, Date.valueOf(LocalDate.now()));
                ps.setString(3, jTextFieldIDNasabahTarik.getText());
                ps.setInt(4, tarik);
                ps.setFloat(5, total);
                ps.executeUpdate();
                conn.commit();
                updateSaldo(jTextFieldIDNasabahTarik.getText(), total);
                insertIntoTabelTrans(Date.valueOf(LocalDate.now()), jTextFieldIDNasabahTarik.getText(), "-", ("-" + tarik), "-", "Penarikan Saldo");
                jTextFieldIDNasabahTarik.setText("");
                jTextFieldJumlahTarik.setText("");
                jLabelNamaTarik.setText("");
                jLabelSaldoTarik.setText("");
                jLabelSaldoAkhir.setText("");
                jButtonTransaksiTarik.setEnabled(false);
                saldo = tarik = total = 0;
                reTarik();
                st.close();
                ps.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jButtonTransaksiTarikActionPerformed

    private void jButtonBackTarikMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBackTarikMouseClicked
        firstTime = true;
        if (modeSekarang.equalsIgnoreCase("light")) {
            cardLayout.show(Points, "transaksiLight");
            trnsCrdsIndx = 1;
        } else {
            cardLayout.show(Points, "transaksiDark");
            trnsCrdsIndx = 2;
        }

        jTextFieldIDNasabahTarik.setText("");
        jTextFieldJumlahTarik.setText("");
        jLabelNamaTarik.setText("");
        jLabelSaldoTarik.setText("");
        jLabelSaldoAkhir.setText("");
        jButtonTransaksiTarik.setEnabled(false);
        saldo = tarik = total = 0;
    }//GEN-LAST:event_jButtonBackTarikMouseClicked

    private void jTextFieldCariNasabahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCariNasabahKeyReleased
        tabelSorter(jTableNasabah, jTextFieldCariNasabah.getText());
    }//GEN-LAST:event_jTextFieldCariNasabahKeyReleased

    private void jTextFieldCariSampahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCariSampahKeyReleased
        tabelSorter(jTableSampah, jTextFieldCariSampah.getText());
    }//GEN-LAST:event_jTextFieldCariSampahKeyReleased

    private void jTextFieldCariNasabahMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldCariNasabahMouseClicked
        jTextFieldCariNasabah.setText("");
    }//GEN-LAST:event_jTextFieldCariNasabahMouseClicked

    private void jTextFieldCariSampahMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldCariSampahMouseClicked
        jTextFieldCariSampah.setText("");
    }//GEN-LAST:event_jTextFieldCariSampahMouseClicked

    private void jButtonBackAboutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonBackAboutMouseClicked
        jMenuLainnya.setVisible(true);
        jMenuLogout.setVisible(true);
        Menu.setVisible(true);
        Points.setVisible(true);

        if (modeSekarang.equalsIgnoreCase("light")) {    
            tampilanDasarLight.setVisible(true);
            tampilanDasarDark.setVisible(true);
        } else {
            tampilanDasarLight.setVisible(false);
            tampilanDasarDark.setVisible(true);
           
        }
        
       switch (menuCrdIndx) {
            case 1:
                firstTime = true;
                cardLayout.show(Points, "transaksiLight");
                break;
            case 2:
                firstTime = true;
                cardLayout.show(Points, "transaksiDark");
                break;
            case 3:
                cardLayout.show(Points, "transaksi1");

                break;
            case 4:
                cardLayout.show(Points, "transaksi2");

                break;
            case 5:
                cardLayout.show(Points, "transaksi3");
                break;
            case 6:
                cardLayout.show(Points, "nasabah");
                break;
            case 7:
                cardLayout.show(Points, "sampah");
                break;
            default:
                break;
        }
       
        jPanelAbout.setVisible(false);
    }//GEN-LAST:event_jButtonBackAboutMouseClicked

    private void jButtonBackAllHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBackAllHistoryActionPerformed
        jMenuItemAbout.setVisible(true);
        Menu.setVisible(true);
        Points.setVisible(true);

        switch (menuCrdIndx) {
            case 1:
                firstTime = true;
                cardLayout.show(Points, "transaksiLight");
                break;
            case 2:
                firstTime = true;
                cardLayout.show(Points, "transaksiDark");
                break;
            case 3:
                cardLayout.show(Points, "transaksi1");

                break;
            case 4:
                cardLayout.show(Points, "transaksi2");

                break;
            case 5:
                cardLayout.show(Points, "transaksi3");
                break;
            case 6:
                cardLayout.show(Points, "nasabah");
                break;
            case 7:
                cardLayout.show(Points, "sampah");
                break;
            default:
                break;
        }

        jPanelAllHistory.setVisible(false);
    }//GEN-LAST:event_jButtonBackAllHistoryActionPerformed

    private void jMenu5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu5MouseClicked

    private void jMenu5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu5ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItemDarkModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDarkModeActionPerformed
        tampilanDasarLight.setVisible(false);
        jPanelMenuLight.setVisible(false);
        tampilanDasarDark.setVisible(true);
        jPanelMenuDark.setVisible(true);

        transaksi_Jual.setBackground(Color.GRAY);
        inputBeli.setBackground(Color.DARK_GRAY);
        showDataBeli.setBackground(Color.DARK_GRAY);
        transaksi_Beli.setBackground(Color.GRAY);
        inputJual.setBackground(Color.DARK_GRAY);
        showDataJual.setBackground(Color.DARK_GRAY);
        transaksi_Tarik.setBackground(Color.GRAY);
        inputTarik.setBackground(Color.DARK_GRAY);
        showDataTarik.setBackground(Color.DARK_GRAY);

        jTableNasabah.setBackground(Color.DARK_GRAY);
        jTableSampah.setBackground(Color.DARK_GRAY);
        jTableHistoryBeli.setBackground(Color.DARK_GRAY);
        jTableHistoryBeli.setForeground(Color.WHITE);
        jTableHistoryJual.setBackground(Color.DARK_GRAY);
        jTableHistoryJual.setForeground(Color.WHITE);
        jTableHistoryTarik.setBackground(Color.DARK_GRAY);
        jTableHistoryTarik.setForeground(Color.WHITE);
        jTextAreaAturanTarik.setBackground(Color.DARK_GRAY);
        jPanelAllHistory.setBackground(Color.BLACK);
        jTableAllHistory.setBackground(Color.GRAY);

        if (firstTime && trnsCrdsIndx == 1) {
            cardLayout.show(Points, "transaksiDark");
            trnsCrdsIndx = 2;
        }
        
        modeSekarang = "dark";

    }//GEN-LAST:event_jMenuItemDarkModeActionPerformed

    private void jMenuItemLightModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLightModeActionPerformed
        tampilanDasarDark.setVisible(false);
        jPanelMenuDark.setVisible(false);
        tampilanDasarLight.setVisible(true);
        jPanelMenuLight.setVisible(true);

        transaksi_Jual.setBackground(Color.decode("#54a4a6"));
        inputBeli.setBackground(Color.decode("#448284"));
        showDataBeli.setBackground(Color.decode("#448284"));
        transaksi_Beli.setBackground(Color.decode("#54a4a6"));
        inputJual.setBackground(Color.decode("#448284"));
        showDataJual.setBackground(Color.decode("#448284"));
        transaksi_Tarik.setBackground(Color.decode("#54a4a6"));
        inputTarik.setBackground(Color.decode("#448284"));
        showDataTarik.setBackground(Color.decode("#448284"));

        jTableNasabah.setBackground(Color.decode("#54a4a6"));
        jTableSampah.setBackground(Color.decode("#54a4a6"));
        jTableHistoryBeli.setBackground(Color.WHITE);
        jTableHistoryBeli.setForeground(Color.BLACK);
        jTableHistoryJual.setBackground(Color.WHITE);
        jTableHistoryJual.setForeground(Color.BLACK);
        jTableHistoryTarik.setBackground(Color.WHITE);
        jTableHistoryTarik.setForeground(Color.BLACK);
        jTextAreaAturanTarik.setBackground(Color.decode("#448284"));
        jPanelAllHistory.setBackground(Color.WHITE);
        jTableAllHistory.setBackground(Color.decode("#54a4a6"));

        if (firstTime && trnsCrdsIndx == 2) {
            cardLayout.show(Points, "transaksiLight");
            trnsCrdsIndx = 1;
        }
        
        modeSekarang = "light";
    }//GEN-LAST:event_jMenuItemLightModeActionPerformed

    private void jButtonBeliDarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBeliDarkActionPerformed
             reTabung();
        cardLayout.show(Points, "transaksi1");
        trnsCrdsIndx = 3;
        menuCrdIndx = 3;
    }//GEN-LAST:event_jButtonBeliDarkActionPerformed

    private void jButtonJualDarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonJualDarkActionPerformed
               reJual();
        cardLayout.show(Points, "transaksi2");
        trnsCrdsIndx = 4;
        menuCrdIndx = 4;
    }//GEN-LAST:event_jButtonJualDarkActionPerformed

    private void jButtonTarikDarkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTarikDarkActionPerformed
               reTarik();
        cardLayout.show(Points, "transaksi3");
        trnsCrdsIndx = 5;
        menuCrdIndx = 5;
    }//GEN-LAST:event_jButtonTarikDarkActionPerformed

    private void jMenuItemHistoriesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHistoriesActionPerformed
             jMenuItemAbout.setVisible(false);
        Menu.setVisible(false);
        Points.setVisible(false);
        
        if (modeSekarang.equalsIgnoreCase("light")) {
            tampilanDasarLight.setVisible(true);
        } else {
            tampilanDasarDark.setVisible(true);
        }
        
        jPanelAllHistory.setVisible(true);
        reAllHistory();
    }//GEN-LAST:event_jMenuItemHistoriesActionPerformed

    private void jMenuItemAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAboutActionPerformed
        // TODO add your handling code here:
        jMenuLainnya.setVisible(false);
        jMenuLogout.setVisible(false);
        Menu.setVisible(false);
        Points.setVisible(false);
         jPanelAllHistory.setVisible(false);
         
        if (modeSekarang.equalsIgnoreCase("light")) {
            tampilanDasarLight.setVisible(false);
            tampilanDasarDark.setVisible(false);
        } else {
            tampilanDasarDark.setVisible(false);
        }
        
       
        jPanelAbout.setVisible(true);

    }//GEN-LAST:event_jMenuItemAboutActionPerformed

    private void jMenuLogoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuLogoutMouseClicked
        int confir = JOptionPane.showConfirmDialog(null, "Apakah anda yakin\n ingin keluar Program?", null, JOptionPane.YES_NO_OPTION);
        if (confir == JOptionPane.YES_OPTION) {
            login = new Login();
            login.setVisible(true);
            this.setVisible(false);
        } else {

        }
    }//GEN-LAST:event_jMenuLogoutMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HOME.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new HOME().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Menu;
    private javax.swing.JPanel Points;
    private javax.swing.JPanel inputBeli;
    private javax.swing.JPanel inputJual;
    private javax.swing.JPanel inputTarik;
    private javax.swing.JButton jButtonBackAbout;
    private javax.swing.JButton jButtonBackAllHistory;
    private javax.swing.JButton jButtonBackBeli;
    private javax.swing.JButton jButtonBackJual;
    private javax.swing.JButton jButtonBackTarik;
    private javax.swing.JButton jButtonBeliDark;
    private javax.swing.JButton jButtonBeliLight;
    private javax.swing.JButton jButtonDeleteSampah;
    private javax.swing.JButton jButtonDeleteUser;
    private javax.swing.JButton jButtonJualDark;
    private javax.swing.JButton jButtonJualLight;
    private javax.swing.JButton jButtonNasabah;
    private javax.swing.JButton jButtonRefreshNasabah;
    private javax.swing.JButton jButtonRefreshSampah;
    private javax.swing.JButton jButtonSampah;
    private javax.swing.JButton jButtonTambahSampah;
    private javax.swing.JButton jButtonTambahUser;
    private javax.swing.JButton jButtonTarikDark;
    private javax.swing.JButton jButtonTarikLight;
    private javax.swing.JButton jButtonTransaksi;
    private javax.swing.JButton jButtonTransaksiBeli;
    private javax.swing.JButton jButtonTransaksiJual;
    private javax.swing.JButton jButtonTransaksiTarik;
    private javax.swing.JLabel jLabel01;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel1HistoryJual;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelBeratSampah;
    private javax.swing.JLabel jLabelHargaBeli;
    private javax.swing.JLabel jLabelHargaJual;
    private javax.swing.JLabel jLabelHistoryBeli;
    private javax.swing.JLabel jLabelHistoryTarik;
    private javax.swing.JLabel jLabelIdSampah;
    private javax.swing.JLabel jLabelJenisBeli;
    private javax.swing.JLabel jLabelJenisJual;
    private javax.swing.JLabel jLabelJumlahTarik;
    private javax.swing.JLabel jLabelNamaBeli;
    private javax.swing.JLabel jLabelNamaPengepul;
    private javax.swing.JLabel jLabelNamaTarik;
    private javax.swing.JLabel jLabelSaldoAkhir;
    private javax.swing.JLabel jLabelSaldoTarik;
    private javax.swing.JLabel jLabelStockJual;
    private javax.swing.JLabel jLabelTotalBeli;
    private javax.swing.JLabel jLabelTotalJual;
    private javax.swing.JLabel jLabelUsernameTarik;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItemAbout;
    private javax.swing.JMenuItem jMenuItemDarkMode;
    private javax.swing.JMenuItem jMenuItemHistories;
    private javax.swing.JMenuItem jMenuItemLightMode;
    private javax.swing.JMenu jMenuLainnya;
    private javax.swing.JMenu jMenuLogout;
    private javax.swing.JMenu jMenuMode;
    private javax.swing.JPanel jPanelAbout;
    private javax.swing.JPanel jPanelAllHistory;
    private javax.swing.JLabel jPanelMenuDark;
    private javax.swing.JLabel jPanelMenuLight;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPaneBeli;
    private javax.swing.JScrollPane jScrollPaneHistoryBeli;
    private javax.swing.JScrollPane jScrollPaneHistoryJual;
    private javax.swing.JScrollPane jScrollPaneNasabah;
    private javax.swing.JScrollPane jScrollPaneSampah;
    private javax.swing.JTable jTableAllHistory;
    private javax.swing.JTable jTableHistoryBeli;
    private javax.swing.JTable jTableHistoryJual;
    private javax.swing.JTable jTableHistoryTarik;
    private javax.swing.JTable jTableNasabah;
    private javax.swing.JTable jTableSampah;
    private javax.swing.JTextArea jTextAreaAturanTarik;
    private javax.swing.JTextField jTextFieldBeratBeli;
    private javax.swing.JTextField jTextFieldBeratJual;
    private javax.swing.JTextField jTextFieldCariNasabah;
    private javax.swing.JTextField jTextFieldCariSampah;
    private javax.swing.JTextField jTextFieldIDNasabahBeli;
    private javax.swing.JTextField jTextFieldIDNasabahTarik;
    private javax.swing.JTextField jTextFieldIDSampahBeli;
    private javax.swing.JTextField jTextFieldIDSampahJual;
    private javax.swing.JTextField jTextFieldJumlahTarik;
    private javax.swing.JTextField jTextFieldNamaPengepulJual;
    private javax.swing.JPanel nasabah;
    private javax.swing.JPanel sampah;
    private javax.swing.JPanel showDataBeli;
    private javax.swing.JPanel showDataJual;
    private javax.swing.JPanel showDataTarik;
    private javax.swing.JLabel tampilanAbout;
    private javax.swing.JLabel tampilanDasarDark;
    private javax.swing.JLabel tampilanDasarLight;
    private javax.swing.JPanel transaksiDark;
    private javax.swing.JPanel transaksiLight;
    private javax.swing.JPanel transaksi_Beli;
    private javax.swing.JPanel transaksi_Jual;
    private javax.swing.JPanel transaksi_Tarik;
    // End of variables declaration//GEN-END:variables

    private void tabelSorter(JTable tabel, String search) {
        rowSorter = new TableRowSorter<>(tabel.getModel());
        tabel.setRowSorter(rowSorter);
        
        if (search.trim().length() == 0) {
            rowSorter.setRowFilter(null);
        } else {
            rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + search));
        }
    }

    private void updateStock(String idSamp, float stockSamp) {
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            st.executeUpdate("UPDATE sampah SET stock = " + stockSamp + " WHERE id_sampah = '" + idSamp + "'");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } finally {
            try {
                conn.commit();
                st.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }

    private void updateSaldo(String idNas, int saldoNas) {
        conn = gConnection.getConnection();
        try {
            st = conn.createStatement();
            st.executeUpdate("UPDATE nasabah SET saldo = " + saldoNas + " WHERE id_nasabah = '" + idNas + "'");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } finally {
            try {
                conn.commit();
                st.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }

    private void insertIntoTabelTrans(Date tanggal, String id_nasabah, String id_sampah, String saldo, String stock, String keterangan) {
        conn = gConnection.getConnection();
        int id_trans;
        try {
            st = conn.createStatement();
            try (ResultSet rs = st.executeQuery("SELECT MAX(id_transaksi) FROM history_transaksi")) {
                if (rs.next()) {
                    id_trans = rs.getInt(1) + 1;
                } else {
                    id_trans = 1;
                }
            }
            ps = conn.prepareStatement("INSERT INTO history_transaksi (id_transaksi, tanggal, id_nasabah, id_sampah, saldo, stock, keterangan) VALUES (?,?,?,?,?,?,?)");
            ps.setInt(1, id_trans);
            ps.setDate(2, tanggal);
            ps.setString(3, id_nasabah);
            ps.setString(4, id_sampah);
            ps.setString(5, saldo);
            ps.setString(6, stock);
            ps.setString(7, keterangan);
            ps.executeUpdate();
            conn.commit();
            st.close();
            ps.close();
            conn.close();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

    public void reSam() {
        DefaultTableModel tb = (DefaultTableModel) jTableSampah.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();
        
        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_sampah, jenis, harga, stock FROM sampah ORDER BY id_sampah ASC");
            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getFloat(4)});
            }
            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void reNas() {
        DefaultTableModel tb = (DefaultTableModel) jTableNasabah.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();
        
        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_nasabah, nama, alamat, saldo FROM nasabah ORDER BY id_nasabah ASC");
            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getInt(4)});
            }
            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void reJual() {
        DefaultTableModel tb = (DefaultTableModel) jTableHistoryJual.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();
        
        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT tanggal, nama_pengepul, id_sampah, berat, total FROM history_jual ORDER BY id_transaksi");
            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getDate(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getFloat(4),
                    rs.getInt(5)});
            }
            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void reTarik() {
        DefaultTableModel tb = (DefaultTableModel) jTableHistoryTarik.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();
        
        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT tanggal, id_nasabah, tarik, saldo_akhir FROM history_tarik ORDER BY id_transaksi");
            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getDate(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4)});
            }
            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void reTabung() {
        DefaultTableModel tb = (DefaultTableModel) jTableHistoryBeli.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();
        
        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT tanggal, id_nasabah, id_sampah, berat FROM history_beli ORDER BY id_transaksi");
            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getDate(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getFloat(4)});
            }
            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void reAllHistory() {
        DefaultTableModel tb = (DefaultTableModel) jTableAllHistory.getModel();
        tb.getDataVector().removeAllElements();
        revalidate();

        try {
            conn = gConnection.getConnection();
            st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT tanggal, id_nasabah, id_sampah, saldo, stock, keterangan FROM history_transaksi ORDER BY id_transaksi");

            while (rs.next()) {
                tb.addRow(new Object[]{
                    rs.getDate(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6)});
            }

            st.close();
            conn.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
